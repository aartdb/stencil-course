class Modal extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open'})
            .innerHTML = `
                <style>
                    #backdrop {
                        z-index: 10;
                        position: fixed;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        background: rgba(0,0,0,.75);
                    }

                    #modal {
                        z-index: 100;
                        position: fixed;
                        top: 15vh;
                        left: 25%;
                        width: 50%;
                        background: white;
                        border-radius: 3px;
                        display: flex;
                        flex-direction: column;
                        justify-content: space-between;
                    }

                    main {
                        padding: 1rem;
                        max-height: 50vh;
                        overflow: auto;
                    }

                    footer {
                        border-top: 1px solid #ccc;
                        padding: 1rem;
                    }

                    footer buttons {

                    }
                </style>

                <div id="backdrop"></div>
                <div id="modal">
                    <header>
                        <slot name="title"></slot>
                    </header>
                    
                    <main>
                        <slot></slot>
                    </main>

                    <footer>
                        <button>Cancel</button>
                        <button>Okay</button>
                    </footer>
                </div>
            `;
    }

}
customElements.define('uc-modal', Modal)