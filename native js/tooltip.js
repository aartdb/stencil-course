class Tooltip extends HTMLElement {
    constructor() {
        super();
        this._tooltipContainer;
        this.attachShadow({ mode: 'open' })
    }

    connectedCallback() {
        this._tooltipContent = this.getAttribute('content') || 'default tooltip content'

        const tooltipIcon = document.createElement('span');
        tooltipIcon.textContent = ' (?)'
        tooltipIcon.addEventListener('mouseenter', this._showTooltip)
        tooltipIcon.addEventListener('mouseleave', this._hideTooltip)
        this.shadowRoot.appendChild(tooltipIcon)
    }

    _showTooltip = () => {
        this._tooltipContainer = document.createElement('div');
        this._tooltipContainer.textContent = this._tooltipContent;
        this.shadowRoot.appendChild(this._tooltipContainer)
    }
    _hideTooltip = () => {
        this.shadowRoot.removeChild(this._tooltipContainer)
    }
}

customElements.define('uc-tooltip', Tooltip)