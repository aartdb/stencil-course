class HideShow extends HTMLElement {
    constructor() {
        super()
        this._visible = false;
        this.attachShadow({ mode: 'open'})
            .innerHTML = `
                <style>
                #info-box {
                    display: ${this._visible ? 'block' : 'none'};
                }
                #info-box.is-visible {
                    display: block;
                }

                ::slotted(.test) {
                    background: red;
                }
                ::slotted(.test2) {
                    background: blue;
                }
                </style>

                <button id="toggle-button">Show</button>
                <div id="info-box">
                    <slot>default</slot>dwadw
                </div>
            `;
    }

    connectedCallback() {
        this._button = this.shadowRoot.querySelector('#toggle-button');
        this._infoBox = this.shadowRoot.querySelector('#info-box');

        this._button.addEventListener('click', e => {
            if (this._visible) {
                this._button.textContent = 'Hide';
                this._infoBox.classList.remove('is-visible')
                this._visible = false;
            } else {
                this._button.textContent = 'Show';
                this._infoBox.classList.add('is-visible')
                this._visible = true;
            }
        })
    }
}

customElements.define('uc-hide-show', HideShow)