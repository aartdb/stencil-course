import { Component, h, Prop, State } from '@stencil/core';

@Component({
    tag: 'uc-tooltip',
    styleUrl: 'tooltip.scss',
    shadow: true
})
export class Tooltip {
    @Prop() content: string;
    @State() tooltipVisible: boolean;

    toggleTooltip(e, test) {
        this.tooltipVisible = !this.tooltipVisible;
        console.log(test)
    }

    render() {
        return [
            <span class="tooltip">
                { this.tooltipVisible ? <div class="content">
                    {this.content}
                </div> : '' }

                <span class="icon" onClick={this.toggleTooltip.bind(this, 'test')}>
                    ?
                </span>
            </span>
        ]
    }
}
