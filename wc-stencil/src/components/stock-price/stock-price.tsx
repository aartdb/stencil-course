import { Component, h, State } from '@stencil/core';

@Component({
    tag: 'uc-stock-price',
    styleUrl: './stock-price.scss',
    shadow: true
})
export class StockPrice {
    @State() fetchedPrice: number;
    key: 'EK4SHCDBFJOVRGQY';

    fetchStockPrice = (event: Event) => {
        event.preventDefault();
        fetch(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=IBM&apikey=demo`)
            .then(res => {
                return res.json();
            })
            .then(res => {
                this.fetchedPrice = +res['Global Quote']['05. price'];
            })
            .catch(console.error)
    }

    render() {
        return [
            <form onSubmit={this.fetchStockPrice}>
                <input class="stock-symbol"/>
                <button type="submit">Fetch</button>
            </form>,
            <div>
                <p>Price: {this.fetchedPrice}</p>
            </div>
        ]
    }
}