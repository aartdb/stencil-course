import { Component, h, Prop } from '@stencil/core';

@Component({
    tag: 'uc-side-drawer',
    styleUrl: './side-drawer.scss',
    shadow: true
})
export class SideDrawer {
    @Prop() heading : string;

    render() {
        return (
            <aside>
                <header><h1>{ this.heading }</h1></header>
                <main>
                    <slot></slot>
                </main>
            </aside>
        )
    }
}