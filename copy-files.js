/**
 * Run this inside your project to clone all currently existing StencilJS components that use the shadow DOM
 * and change them to using the scoped functionality.
 * 
 * Simply run `$ node copy-files.js` //TODO better name would be 'scopify-components'
 */

const fs = require('fs');
const path = require('path');
const util = require('util');

const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const readdir = util.promisify(fs.readdir);
const stat = util.promisify(fs.stat);

const componentsPath = `wc-stencil/src/components`;
const nativeSuffix = 'Scoped'; // make sure this is Capitalized
const fileExt = `.tsx`;

(async function () {
    const files = await searchDirsRecursive(componentsPath, fileExt);

    for (const file of files) {
        scopifyFile(file)
    }
})()

/**
 * Copy the original file to a file that is a scoped version
 * @param {string} file 
 * @returns {void} new file
 */
async function scopifyFile(file) {
    const originalContent = await readFile(file, 'utf8');

    // deep copy the text - we don't want to edit the original. This solves a known issue: https://code.google.com/p/v8/issues/detail?id=2869
    let newContent = (' ' + originalContent).slice(1);

    // first, get the @Component decorator
    const componentRegex = /@Component.*}\)/gms;
    const componentDecorator = componentRegex.exec(newContent);

    if (!(componentDecorator && componentDecorator[0])) return console.warn(`No @Component decorator found @ ${file}`)

    // and replace the original component decorator with this IIFE
    newContent = newContent.replace(componentRegex, (
        (text) => {
            // change the tag
            const tagRegex = /(tag:)(.*)(')/gm;
            text = text.replace(tagRegex, `$1$2-${nativeSuffix.toLowerCase()}$3`);

            // change shadow to scoped
            const shadowRegex = /shadow:(\s*)true/gm;
            text = text.replace(shadowRegex, `scoped: true`);
            return text;
        }
    )(componentDecorator[0]));

    // Change the class' name
    const classNameRegex = /(export class )(.*)( {)/gm;
    newContent = newContent.replace(classNameRegex, `$1$2${nativeSuffix}$3`);

    // Give a warning if an id is used somewhere in the template
    const templateRegex = /render.*(})/gms;
    const template = templateRegex.exec(newContent);
    if (template[0].includes('id=')) console.warn(`Watch out - you shouldn't use an id in the template @ ${file}`)

    // And save it as a new file
    const scopifiedFileName = file.replace(new RegExp(`(${fileExt})`), `-${nativeSuffix.toLowerCase()}$1`);
    return await writeFile(scopifiedFileName, newContent, 'utf8');
}

/**
 * Search the dirs and underlying dirs for a pattern
 * @param {string} dir 
 * @param {string} pattern - for example: '.tsx'
 * @returns {string[]} an array of filenames
 */
async function searchDirsRecursive(dir, pattern) {
    var results = [];
    const dirs = await readdir(dir);

    for (const dirInner of dirs) {
        // Obtain absolute path
        const absPath = path.resolve(dir, dirInner);

        // Get stats to determine if path is a directory or a file
        const pathStat = await stat(absPath);

        // If path is a directory, scan it and combine results
        if (pathStat.isDirectory()) {
            results = results.concat(await searchDirsRecursive(absPath, pattern));
        }

        // If path is a file and ends with pattern then push it onto results
        if (pathStat.isFile() && absPath.endsWith(pattern)) {
            // skip it if it's already the scoped version
            if (absPath.endsWith(`${nativeSuffix.toLowerCase()}${pattern}`)) continue;

            results.push(absPath);
        }
    }

    return results;
};